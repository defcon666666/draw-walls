﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GoalView : MonoBehaviour
{
    [SerializeField] private Text currentText;
    [SerializeField] private Text maxText;
    [SerializeField] private Animator animator;
    [SerializeField] private Slider slider;
    public void SetValue(int current, int max)
    {
        currentText.text = current.ToString();
        maxText.text ="/"+ max;
        slider.value = current;
        slider.maxValue = max;
        animator.SetTrigger("Shake");
    }
}
