﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    public int maxBallSpawn = 10;
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private Ball ballPrefab;

    public void Init()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        for (int i = 0; i < maxBallSpawn; i++)
        {
            yield return new WaitForSeconds(0.2f);
            Ball localBall= Instantiate(ballPrefab, spawnPoint.position, spawnPoint.rotation);
            localBall.Init();
            localBall.Falling();
        }
    }
}
