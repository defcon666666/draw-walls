﻿using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class FluidHole : MonoBehaviour
{
    public ObiSolver solver;
    public ObiCollider holeCollider = null;
    public GoalView goal;
    private int currentEnterPart=0;
    private int maxPart=250;

    void OnEnable () {
        solver.OnCollision += Solver_OnCollision;
        goal.SetValue(currentEnterPart,maxPart);
    }

    void OnDisable(){
        solver.OnCollision -= Solver_OnCollision;
    }
    
    void Solver_OnCollision (object sender, Obi.ObiSolver.ObiCollisionEventArgs e)
    {
        var colliderWorld = ObiColliderWorld.GetInstance();
        
        for (int i = 0;  i < e.contacts.Count; ++i)
        {
            if (e.contacts.Data[i].distance < 0.01f)
            {
                var col = colliderWorld.colliderHandles[e.contacts.Data[i].other].owner;
                
                if (col == holeCollider)
                {
                    PointUp();
                    return;
                }
            }
        }
    }

    private void PointUp()
    {
        if(currentEnterPart>=maxPart)
            return;

        currentEnterPart++;
        goal.SetValue(currentEnterPart,maxPart);
    }
}
