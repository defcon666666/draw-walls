﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private GoalView goalView;
    [SerializeField] private GameObject check;
    private int maxBall=15;
    private int currentBall;

    public void Start()
    {
        goalView.SetValue(currentBall,maxBall);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Impact();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            Impact();
        }
    }

    private void Impact()
    {
        if(currentBall>=maxBall)
            return;
            
        currentBall++;
        goalView.SetValue(currentBall,maxBall);
        animator.SetTrigger("Shake");

        check.SetActive(currentBall>=maxBall);
    }
}
