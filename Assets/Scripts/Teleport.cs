﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    [SerializeField] private TeleportLogick type;
    [SerializeField] private Teleport outTeleport;
    public enum TeleportLogick
    {
        In,
        Out
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            if (type == TeleportLogick.In)
            {
                Ball ball = other.gameObject.GetComponent<Ball>();
                Vector3 offset = ball.transform.position - transform.position;
                float angle = Vector3.SignedAngle(transform.forward, outTeleport.transform.forward, Vector3.up);
                Quaternion quaternion = Quaternion.AngleAxis(angle,Vector3.up);
                
                ball.transform.position = outTeleport.transform.position + (quaternion* offset);
                ball.Turn(outTeleport.transform.forward);
            }
        }
    }
}
