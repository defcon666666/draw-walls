﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeHole : MonoBehaviour
{
    public bool isMove = false;
    public Transform point1;
    public Transform point2;
    [SerializeField] private GoalView goalView;
    private Vector3 targetMove;
    private int maxCount = 30;
    private int currentCount;
    private bool isSwipeTarget;
    
    public void Start()
    {
        goalView.SetValue(currentCount,maxCount);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentCount++;
            goalView.SetValue(currentCount,maxCount);
        }
        
        if(!isMove)
            return;
        transform.position = Vector3.Lerp(transform.position, targetMove, Time.deltaTime);

        if (Vector3.Distance(transform.position, targetMove) < 0.1f)
        {
            if (isSwipeTarget)
                targetMove = point1.position;
            else
                targetMove = point2.position;
            
            isSwipeTarget = !isSwipeTarget;
        }
    }
}
