﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{
    [SerializeField] private GameObject model;
    [SerializeField] private MeshRenderer renderer;
    private float y = 0.25f;
    private float speed = 0.45f;
    public void Start()
    {
        StartCoroutine(UpMoveModel());
    }

    public void SetColor(Color c)
    {
        renderer.material.color = c;
    }

    private IEnumerator UpMoveModel()
    {
        while (model.transform.localPosition.y<y) 
        {
            model.transform.position+=Vector3.up*Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
