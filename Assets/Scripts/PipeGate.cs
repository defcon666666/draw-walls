﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeGate : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject gateCollider;
    private bool isOpen;
    
    public void OpenGate()
    {
        isOpen = true;
        anim.SetBool("Open",isOpen);
        gateCollider.SetActive(!isOpen);
        
    }
}
