﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    public bool isMove = false;
    public Transform point1;
    public Transform point2;
    [SerializeField] private GoalView goalView;
    [SerializeField] private Light light;
    private Vector3 targetMove;
    private bool isSwipeTarget;
    private int currentFallBall=0;
    private int maxBall=20;
    public void Start()
    {
        if(!isMove)
            return;
        targetMove = point1.position;
    }

    public void Update()
    {
        if(!isMove)
            return;


        transform.position = Vector3.Lerp(transform.position, targetMove, Time.deltaTime);

        if (Vector3.Distance(transform.position, targetMove) < 0.1f)
        {
            if (isSwipeTarget)
                targetMove = point1.position;
            else
                targetMove = point2.position;
            
            isSwipeTarget = !isSwipeTarget;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            currentFallBall++;
            goalView.SetValue(currentFallBall,maxBall);
            light.enabled = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            other.gameObject.layer = LayerMask.NameToLayer("Ignore Default");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        light.enabled = false;
    }
}
