﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    [SerializeField] private MeshRenderer renderer;
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private bool isRandom=false;
    private float speed=0;
    private float accelerat=1.01f;
    private bool isInit = false;
    
    public void Init()
    {
        if(isRandom)
            renderer.material.color = GameManager.instance.GetRandomBallColor();
        
        rigidbody.isKinematic = true;

        isInit = true;
    }

    public void Update()
    {
        Debug.DrawRay(transform.position,rigidbody.velocity,Color.red,Time.deltaTime);
    }

    public void Falling()
    {
        if(!isInit)
            return;
        
        rigidbody.isKinematic = false;
        rigidbody.AddForce(transform.forward*6,ForceMode.Impulse);
    }

    public void Turn(Vector3 dir)
    {
        float magnitude = rigidbody.velocity.magnitude;
        rigidbody.velocity = dir.normalized * magnitude;
    }
}
