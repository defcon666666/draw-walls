﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBuilder : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private GameObject field;
    [SerializeField] private Segment segment;
    [Space(10)] 
    [SerializeField] private int colorIndex;
    
    private List<Vector3> points;
    private RaycastHit hit;
    private Ray ray;
    private float interval = 0.18f;
    private bool isFirstColor = true;
    
    public void Update()
    {
        #if UNITY_EDITOR || UNITY_STANDALONE_WIN
            if (Input.GetMouseButtonDown(0))
            {
                DownMouse();
            }
            if (Input.GetMouseButton(0))
            {
                DragMouse();
            }
            if (Input.GetMouseButtonUp(0))
            {
                UpMouse();
            }
        #endif
        
        #if UNITY_IOS || UNITY_ANDROID
            if (Input.touches.Length != 0)
            {
                Touch touch = Input.touches[0];
                if (touch.phase == TouchPhase.Began)
                {
                    DownMouse();
                }

                if (touch.phase == TouchPhase.Moved)
                {
                    DragMouse();
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    UpMouse();
                }
            }
        #endif
    }

    private void UpMouse()
    {
        GameManager.instance.StartFallingBall();
    }

    private void DownMouse()
    {
        points = new List<Vector3>();
        points.Add(CalculatePosition());
    }

    private void DragMouse()
    {
        Vector3 newPos = CalculatePosition();
        Vector3 lastPos = points[points.Count - 1];
        Vector3 dir = newPos - lastPos;
        float dist = Vector3.Distance(lastPos,newPos);
        
        Quaternion rotation = Quaternion.LookRotation(dir,field.transform.up);
        
        if (dist>interval)
        {
            newPos = lastPos + dir.normalized * interval;
            points.Add(newPos);
            
            Instantiate(segment, newPos, rotation).SetColor(GameManager.instance.GetSegmentColor(colorIndex,isFirstColor));
            isFirstColor = !isFirstColor;
        }
    }

    private Vector3 CalculatePosition()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
            return  hit.point;
        
        return Vector3.zero;
    }
}
