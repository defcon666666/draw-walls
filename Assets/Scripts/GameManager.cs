﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    [SerializeField] private GameObject levelGameObject;
    [SerializeField] private Color[] ballColors;
    [SerializeField] private WallColor[] wallColors;
    [SerializeField] private Pipe[] pipes;
    [SerializeField] private PipeGate[] pipesGate;
    [SerializeField] private GameObject waterGate;
    
    private List<Ball> allBallInLevel = new List<Ball>();
    private bool isFirstStart = true;
    
    [Serializable] public class WallColor
    {
        public Color color1;
        public Color color2;

        public Color GetColor(bool isFirst)
        {
            if (isFirst)
                return color1;
            else
                return color2;
        }
    }
    private void Awake()
    {
        instance = this;
        Init();
    }

    private void Init()
    {
        allBallInLevel = levelGameObject.GetComponentsInChildren<Ball>().ToList();
        
        if(allBallInLevel.Count!=0)
            foreach (var ball in allBallInLevel)
            {
                ball.Init();
            }
    }

    public Color GetSegmentColor(int i,bool isFirst)
    {
        return wallColors[i].GetColor(isFirst);
    }

    public void StartFallingBall()
    {
        if(!isFirstStart)
            return;

        foreach (var ball in allBallInLevel)
        {
            ball.Falling();
        }

        if (pipes != null)
        {
            foreach (var pipe in pipes)
            {
                pipe.Init();
            }
        }

        if (pipesGate.Length != 0)
        {
            foreach (var pipeGate in pipesGate)
            {
                pipeGate.OpenGate();
            }
        }

        if(waterGate)
            waterGate.SetActive(false);
        
        isFirstStart = false;
    }

    public Color GetRandomBallColor()
    {
        return ballColors[Random.Range(0, ballColors.Length)];
    }
}
