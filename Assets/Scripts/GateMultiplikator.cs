﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateMultiplikator : MonoBehaviour
{
    [SerializeField] private Ball ballPrefab;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            Ball localBall= Instantiate(ballPrefab, other.gameObject.transform.position, other.gameObject.transform.rotation);
            localBall.Init();
            localBall.Falling();
        }
    }
}
